﻿using System;
using System.IO;
using System.Numerics;
using System.Collections.Generic;
using System.Text;

namespace Elliptic_curve
{
    class RSA
    {
        public void CreateKeys(int bits)
        {
            BigInteger p;
            do
            {
                p = GetPrimeOverBits(bits);
            } while (p % 6 != 5);

            Console.WriteLine(" p = " + p);

            BigInteger q;
            do
            {
                q = GetPrimeOverBits(bits);
            } while (q % 6 != 5 || q == p);
            
            Console.WriteLine(" q = " + q);

            BigInteger n = p * q;

            Console.WriteLine(" n = " + n);

            BigInteger testNum = (p + 1) * (q + 1);

            BigInteger e;
            Random rand = new Random();

            string str = "";
            for (int i = 0; i < n.ToString().Length; i++)
            {
                str += rand.Next() % 10;
            }
            e = BigInteger.Parse(str);
            e %= n;
            BigInteger[] gcd = Extended_GCD(e, testNum);
            while (gcd[0] != 1)
            {
                e = (e + 1) % n;
                if (e < 2) { e = 2; }
                gcd = Extended_GCD(e, testNum);
            }

            BigInteger d = (gcd[1] + testNum) % testNum;

            File.WriteAllText("PublicKey", n + "\n" + e);
            File.WriteAllText("PrivateKey", n + "\n" + d);

            Console.WriteLine(" Файлы ключей созданы");
        }

        public void Encrypt(string iFile, string oFile, string keyFile)
        {
            string[] lines = File.ReadAllLines(keyFile);
            BigInteger n = BigInteger.Parse(lines[0]);
            BigInteger e = BigInteger.Parse(lines[1]);

            byte[] file = File.ReadAllBytes(iFile);

            StringBuilder ret = new StringBuilder();

            List<BigInteger> blocks = GetBlocks(file, n);
            Random rand = new Random();
            foreach (BigInteger m in blocks)
            {
                BigInteger y;
                string str = "";
                for (int j = 0; j < n.ToString().Length; j++)
                    str += rand.Next() % 10;

                y = BigInteger.Parse(str);
                y %= n;
                
                Point My = new Point(m, y, n);
                Point C = My * e;
                ret.Append(C + "\n");
            }

            File.WriteAllText(oFile, ret.ToString());

            Console.WriteLine(" Файл зашифрован");
        }

        public void Decrypt(string iFile, string oFile, string keyFile)
        {
            string[] lines = File.ReadAllLines(keyFile);
            BigInteger n = BigInteger.Parse(lines[0]);
            BigInteger d = BigInteger.Parse(lines[1]);

            string[] ccc = File.ReadAllLines(iFile);
            List<Point> points = new List<Point>();
            for (int i = 0; i < ccc.Length; i++)
            {
                string[] str = ccc[i].Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                Point C = new Point(BigInteger.Parse(str[0]), BigInteger.Parse(str[1]), n);
                Point My = C * d;
                points.Add(My);
            }

            byte[] file = GetBytes(points);

            BinaryWriter STG = new BinaryWriter(File.Open(oFile, FileMode.Create));
            foreach (byte b in file)
            {
                STG.Write(b);
            }

            STG.Close();
            
            Console.WriteLine(" Файл расшифрован");
        }

        private List<BigInteger> GetBlocks(byte[] arr, BigInteger n)
        {
            List<BigInteger> res = new List<BigInteger>();

            StringBuilder str = new StringBuilder();

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] / 100 > 0)
                {
                    str.Append(arr[i]);
                }
                else if (arr[i] / 10 > 0)
                {
                    str.Append("0" + arr[i]);
                }
                else 
                {
                    str.Append("00" + arr[i]);
                }
            }

            string message = str.ToString();
            int bl = (n.ToString().Length % 3 == 0) ? n.ToString().Length - 3 : (n.ToString().Length / 3) * 3;
            for (int i = 0; i < message.Length; i += bl)
            {
                int j = (i + bl < message.Length) ? bl : message.Length - i;
                res.Add(BigInteger.Parse(message.Substring(i, j)));
            }

            return res;
        }

        private byte[] GetBytes(List<Point> arr)
        {
            StringBuilder str = new StringBuilder();
            for (int  i = 0; i < arr.Count; i++)
            {
                if (arr[i].x.ToString().Length % 3 == 0)
                {
                    str.Append(arr[i].x.ToString());
                }
                else if (arr[i].x.ToString().Length % 3 == 1)
                {
                    str.Append( "00" + arr[i].x.ToString());
                }
                else if (arr[i].x.ToString().Length % 3 == 2)
                {
                    str.Append("0" + arr[i].x.ToString());
                }
            }

            string message = str.ToString();
            byte[] res = new byte[message.Length / 3];
            int j = 0;
            for (int i = 0; i < res.Length; i++)
            {
                res[i] = byte.Parse(message.Substring(j, 3));
                j += 3;
            }

            return res;

        }


        private BigInteger GetStartPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            if (bits % 8 == 0)
            {
                tmp[bits / 8 - 1] = 0x80;
            }
            else
            {
                tmp[bits / 8] = (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetEndPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = 0xFF;
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((1 << (bits % 8)) - 1);
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetRandomPosition(int bits)
        {
            Random rand = new Random();

            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = (byte)(rand.Next() % 0xFF);
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((rand.Next() % ((1 << (bits % 8)) - 1)));
                tmp[bits / 8] |= (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetPrimeOverBits(int bits)
        {
            BigInteger startInterval = GetStartPosition(bits);
            BigInteger endInterval = GetEndPosition(bits);
            BigInteger startBusting = GetRandomPosition(bits);

            for (BigInteger i = startBusting; i < endInterval; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }
            for (BigInteger i = startInterval; i < startBusting; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }

            return BigInteger.Parse("-1");
        }

        private bool IsPrime(BigInteger p)
        {
            BigInteger p1 = p - 1;

            int s = 0;

            BigInteger t = p1;
            while (t % 2 == 0)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 30; i++)
            {
                Random rand = new Random();
                BigInteger tmp = 0;
                while (tmp > p1 || tmp < 2)
                {
                    string str = "";
                    for (int j = 0; j < p1.ToString().Length; j++)
                        str += rand.Next() % 10;

                    tmp = BigInteger.Parse(str);
                    tmp %= p;
                }

                BigInteger x = BigInteger.ModPow(tmp, t, p);

                if (x <= 1 || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInteger.ModPow(x, 2, p);

                    if (x == 1)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }

        public static BigInteger[] Extended_GCD(BigInteger A, BigInteger B)
        {
            BigInteger[] result = new BigInteger[3];
            bool reverse = false;
            if (A < B)
            {
                BigInteger temp = A;
                A = B;
                B = temp;
                reverse = true;
            }
            BigInteger r = B;
            BigInteger q = 0;
            BigInteger x0 = 1;
            BigInteger y0 = 0;
            BigInteger x1 = 0;
            BigInteger y1 = 1;
            BigInteger x = 0, y = 0;
            while (A % B != 0)
            {
                r = A % B;
                q = A / B;
                x = x0 - q * x1;
                y = y0 - q * y1;
                x0 = x1;
                y0 = y1;
                x1 = x;
                y1 = y;
                A = B;
                B = r;
            }
            result[0] = r;
            if (reverse)
            {
                result[1] = y;
                result[2] = x;
            }
            else
            {
                result[1] = x;
                result[2] = y;
            }
            return result;
        }
    }
}
