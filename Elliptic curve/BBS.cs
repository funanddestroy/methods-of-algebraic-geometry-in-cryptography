﻿using System;
using System.IO;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elliptic_curve
{
    class BBS
    {
        private BigInteger p;
        private BigInteger q;
        private BigInteger n;
        private BigInteger N;
        private Point P;
        private int bits;

        private Random rand = new Random();

        public BBS(int bits)
        {
            this.bits = bits;

            do
            {
                p = GetPrimeOverBits(bits);
            } while (!((p % 6 == 5))); //&& IsPrime((p + 1) / 2)));

            Console.WriteLine("p = " + p); 

            do
            {
                q = GetPrimeOverBits(bits);
            } while (!((q % 6 == 5) && (q != p))); //&& IsPrime((q + 1) / 2)));

            Console.WriteLine("q = " + q);

            n = p * q;
            Console.WriteLine("n = " + n);

            N = (p + 1) * (q + 1);
            Console.WriteLine("N = " + N);

            string str = "";
            for (int j = 0; j < n.ToString().Length; j++)
                str += rand.Next() % 10;

            BigInteger x = BigInteger.Parse(str) % n;

            str = "";
            for (int j = 0; j < n.ToString().Length; j++)
                str += rand.Next() % 10;

            BigInteger y = BigInteger.Parse(str) % n;
            P = new Point(x, y, n);

            Console.WriteLine("P = " + P);
        }

        public BigInteger GetNext(int b = 128)
        {
            byte[] tmp = new byte[b / 8 + 1];

            for (int i = 0; i < b; i++)
            {
                tmp[i / 8] |= (byte)((P.x.ToByteArray()[0] & 1) << (i % 8));

                P = P * 2;
            }

            return new BigInteger(tmp);
        }


        private BigInteger GetStartPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            if (bits % 8 == 0)
            {
                tmp[bits / 8 - 1] = 0x80;
            }
            else
            {
                tmp[bits / 8] = (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetEndPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = 0xFF;
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((1 << (bits % 8)) - 1);
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetRandomPosition(int bits)
        {
            Random rand = new Random();

            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = (byte)(rand.Next() % 0xFF);
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((rand.Next() % ((1 << (bits % 8)) - 1)));
                tmp[bits / 8] |= (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetPrimeOverBits(int bits)
        {
            BigInteger startInterval = GetStartPosition(bits);
            BigInteger endInterval = GetEndPosition(bits);
            BigInteger startBusting = GetRandomPosition(bits);

            for (BigInteger i = startBusting; i < endInterval; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }
            for (BigInteger i = startInterval; i < startBusting; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }

            return BigInteger.Parse("-1");
        }

        private bool IsPrime(BigInteger prime)
        {
            BigInteger p1 = prime - 1;

            int s = 0;

            BigInteger t = p1;
            while (t % 2 == 0)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 30; i++)
            {
                Random rand = new Random();
                BigInteger tmp = 0;
                while (tmp > p1 || tmp < 2)
                {
                    string str = "";
                    for (int j = 0; j < p1.ToString().Length; j++)
                        str += rand.Next() % 10;

                    tmp = BigInteger.Parse(str);
                    tmp %= prime;
                }

                BigInteger x = BigInteger.ModPow(tmp, t, prime);

                if (x <= 1 || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInteger.ModPow(x, 2, prime);

                    if (x == 1)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
