﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elliptic_curve
{
    class Point
    {
        public BigInteger x;
        public BigInteger y;
        public BigInteger p;
        public bool inf;

        public Point(BigInteger x, BigInteger y, BigInteger p, bool inf)
        {
            this.x = x;
            this.y = y;
            this.p = p;
            this.inf = inf;
        }

        public Point(BigInteger x, BigInteger y, BigInteger p)
        {
            this.x = x;
            this.y = y;
            this.p = p;
        }
                
        public override string ToString()
        {
            return x + " " + y;
        }

        public static Point operator +(Point A, Point B)
        {
            Point C;
            if (A.inf == true)
            {
                C = new Point(B.x, B.y, B.p, B.inf);
            }
            else if (B.inf == true)
            {
                C = new Point(A.x, A.y, A.p, A.inf);
            }
            else if (A.x == B.x && ((A.y == 0 && B.y == 0) || (A.y != B.y)))
            {
                C = new Point(0, 0, A.p, true);
            }
            else if (A.x == B.x && A.y == B.y)
            {
                BigInteger lambda;
                BigInteger lambdaTop = BigInteger.Pow(A.x, 2) * 3;
                BigInteger lambdaBottom = A.y * 2;

                BigInteger gcd = BigInteger.GreatestCommonDivisor(BigInteger.Abs(lambdaTop), BigInteger.Abs(lambdaBottom));
                lambdaTop = lambdaTop / gcd;
                lambdaBottom = lambdaBottom/ gcd;

                if (lambdaBottom < 0)
                {
                    lambdaTop = BigInteger.Negate(lambdaTop);
                    lambdaBottom = BigInteger.Negate(lambdaBottom);
                }

                if (lambdaBottom == 1)
                {
                    lambdaTop = lambdaTop % A.p;
                    lambda = lambdaTop >= 0 ? lambdaTop : lambdaTop + A.p;
                }
                else if (lambdaBottom == 0)
                {
                    return new Point(0, 0, A.p, true);
                }
                else
                {
                    lambda = (lambdaTop * EllipticСurve.Extended_GCD(lambdaBottom, A.p)[1]) % A.p;
                    lambda = lambda >= 0 ? lambda : lambda + A.p;
                }
                BigInteger x3 = (BigInteger.Pow(lambda, 2) - 2 * A.x) % A.p; 
                x3 = x3 >= 0 ? x3 : x3 + A.p;
                BigInteger y3 = ((lambda * (A.x - x3)) - A.y) % A.p;  
                y3 = y3 >= 0 ? y3 : y3 + A.p;
                C = new Point(x3, y3, A.p);
            }
            else
            {
                BigInteger lambda;
                BigInteger lambdaTop = B.y - A.y;
                BigInteger lambdaBottom = B.x - A.x;

                BigInteger gcd = BigInteger.GreatestCommonDivisor(BigInteger.Abs(lambdaTop), BigInteger.Abs(lambdaBottom));
                lambdaTop = lambdaTop / gcd;
                lambdaBottom = lambdaBottom / gcd;

                if (lambdaBottom < 0)
                {
                    lambdaTop = BigInteger.Negate(lambdaTop);
                    lambdaBottom = BigInteger.Negate(lambdaBottom);
                }

                if (lambdaBottom == 1)
                {
                    lambdaTop = lambdaTop % A.p;
                    lambda = lambdaTop >= 0 ? lambdaTop : lambdaTop + A.p;
                }
                else if (lambdaBottom == 0)
                {
                    return new Point(0, 0, A.p, true);
                }
                else
                {
                    lambda = (lambdaTop * EllipticСurve.Extended_GCD(lambdaBottom, A.p)[1]) % A.p;
                    lambda = lambda >= 0 ? lambda : lambda + A.p;
                }

                BigInteger x3 = (BigInteger.Pow(lambda, 2) - B.x - A.x) % A.p; 
                x3 = x3 >= 0 ? x3 : x3 + A.p;
                BigInteger y3 = (lambda * (A.x - x3) - A.y) % A.p; 
                y3 = y3 >= 0 ? y3 : y3 + A.p;
                C = new Point(x3, y3, A.p);
            }
            return C;
        }

        public static Point operator *(Point A, BigInteger N)
        {
            if (N == 1) return A;
            Point B = new Point(A.x, A.y, A.p, A.inf);
            BigInteger n = 2;
            int i = 1;
            while (n <= N)
            {
                n *= 2;
                i++;
            }
            if (i > 1)
            {
                i--;
                n /= 2;
            }
            for (int j = 0; j < i; j++)
            {
                B = B + B;
            }
            BigInteger n1;
            if (n < N)
            {
                n1 = N - n;
                B = B + (A * n1);
            }
            return B;
        }
    }
}
