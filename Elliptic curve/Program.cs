﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Elliptic_curve
{
    class Program
    {
        static void Main(string[] args)
        {
            /*RSA rsa = new RSA();

            string help = "genkeys\t- сгенерировать ключи;\n" +
                          "enc\t- зашифровать файл;\n" +
                          "dec\t- расшифровать файл;\n" +
                          "exit\t- выход из программы.\n" +
                          "\n";
            Console.Write(help);


            string pr = " > ";

            while (true)
            {
                Console.Write(pr);
                string command = Console.ReadLine();

                switch (command)
                {
                    case "genkeys":
                        Console.WriteLine(" Введите количество бит: ");
                        Console.Write(pr);
                        int bits = int.Parse(Console.ReadLine());
                        rsa.CreateKeys(bits);
                        break;

                    case "enc":
                        Console.WriteLine(" Public key: ");
                        Console.Write(pr);
                        string key = Console.ReadLine();
                        Console.WriteLine(" Введите имя файла: ");
                        Console.Write(pr);
                        string file = Console.ReadLine();
                        Console.WriteLine(" Введите имя файла для шифрования: ");
                        Console.Write(pr);
                        string ofile = Console.ReadLine();
                        rsa.Encrypt(file, ofile, key);
                        break;

                    case "dec":
                        Console.WriteLine(" Private key: ");
                        Console.Write(pr);
                        key = Console.ReadLine();
                        Console.WriteLine(" Введите имя зашифрованного файла: ");
                        Console.Write(pr);
                        file = Console.ReadLine();
                        Console.WriteLine(" Введите имя файла для расшифрования: ");
                        Console.Write(pr);
                        ofile = Console.ReadLine();
                        rsa.Decrypt(file, ofile, key);
                        break;

                    case "exit":
                        return;
                }
            }*/

            BBS bbs = new BBS(10);

            StringBuilder s = new StringBuilder();
            for (int i = 0; i < 1000; i++)
            {
                s.Append(bbs.GetNext(10) + " ");
            }

            File.WriteAllText("output", s.ToString());

        }
    }
}
