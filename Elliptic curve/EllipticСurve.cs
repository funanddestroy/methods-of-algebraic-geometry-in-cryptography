﻿using System;
using System.IO;
using System.Numerics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elliptic_curve
{
    class EllipticСurve
    {
        public void GenerateEllipticСurve(int bits)
        {
            BigInteger p, c, d, r = 0, N = 0;
            bool f = true, ff = false;

            do
            {
                while (true)
                {
                    p = GetCharPrime(bits);
                    BigInteger[] cd = FactorPrime(p);
                    if (cd == null) continue;

                    c = cd[0];
                    d = cd[1];

                    for (int i = 0; i < 6; i++)
                    {
                        BigInteger T = 0;

                        switch (i)
                        {
                            case 0:
                                T = c + 3 * d;
                                break;
                            case 1:
                                T = BigInteger.Negate(c + 3 * d);
                                break;
                            case 2:
                                T = c - 3 * d;
                                break;
                            case 3:
                                T = BigInteger.Negate(c - 3 * d);
                                break;
                            case 4:
                                T = 2 * c;
                                break;
                            default:
                                T = BigInteger.Negate(2 * c);
                                break;
                        }

                        N = p + 1 + T;
                        r = 0;
                        if (IsPrime(N))
                        {
                            r = N;
                            ff = true;
                            break;
                        }
                        else if (N % 2 == 0 && IsPrime(N / 2))
                        {
                            r = N / 2;
                            ff = true;
                            break;
                        }
                        else if (N % 3 == 0 && IsPrime(N / 3))
                        {
                            r = N / 3;
                            ff = true;
                            break;
                        }
                        else if (N % 6 == 0 && IsPrime(N / 6))
                        {
                            r = N / 6;
                            ff = true;
                            break;
                        }
                    }
                    if (ff) break;                    
                }

                if (p == r) f = false;
                int m = 3; 
                for (int i = 1; i <= m; i++)
                {
                    if (BigInteger.ModPow(p, i, r) == 1)
                    {
                        f = true;
                        break;
                    }
                }

            } while (false);

            Console.WriteLine("p = " + p);
            Console.WriteLine("c = " + c);
            Console.WriteLine("d = " + d);
            Console.WriteLine("N = " + N);
            Console.WriteLine("r = " + r);
            Point p_inf, p0;
            BigInteger B, x0, y0;

            do
            {
                BigInteger[] bx0y0 = GetB(p, N, r);
                B = bx0y0[0];
                x0 = bx0y0[1];
                y0 = bx0y0[2];
                p0 = new Point(x0, y0, p, false);
                
                p_inf = p0 * N;
            } while (!p_inf.inf);
            Console.WriteLine("B = " + B);
            Console.WriteLine("x0 = " + x0);
            Console.WriteLine("y0 = " + y0);

            Point Q = p0 * (N / r);
            Console.WriteLine("Q = (" + Q.x + ", " + Q.y + ")");

            PrintPoints(Q, r);
        }

        private void PrintPoints(Point Q, BigInteger r)
        {
            string outX = "";
            string outY = "";
            for (BigInteger i = 1; i <= r; i++)
            {
                Point p = Q * i;
                string X = p.x + "";
                string Y = p.y + "";
                outX += X + "\n";
                outY += Y + "\n";
            }

            File.WriteAllText("X.txt", outX);
            File.WriteAllText("Y.txt", outY);
        }

        private BigInteger GetCharPrime(int bits)
        {
            BigInteger p;
            do
            {
                p = GetPrimeOverBits(bits);
            } while (p % 6 != 1);
            return p;
        }

        private BigInteger[] FactorPrime(BigInteger p)
        {
            BigInteger[] res = new BigInteger[2];
            BigInteger D = 3;

            BigInteger mD = p - D;
            if (QuadraticNonResidue(mD, p)) return null;

            BigInteger u = Nechto(mD, p);
            List<BigInteger> uu = new List<BigInteger>();
            List<BigInteger> mm = new List<BigInteger>();
            uu.Add(u);
            mm.Add(p);

            int i = 0;
            do
            {
                BigInteger nextM = (BigInteger.Pow(uu[i], 2) + D) / mm[i]; 
                mm.Add(nextM);

                BigInteger xxx = uu[i] % mm[i + 1]; 
                BigInteger yyy = mm[i + 1] - xxx; 
                BigInteger nextU = xxx > yyy ? yyy : xxx;

                uu.Add(nextU);
                i++;
                if (mm[i] == 4)
                { 
                    return null;
                }
            } while (mm[i] != 1);
            i--;

            BigInteger[] aa = new BigInteger[i + 1];
            BigInteger[] bb = new BigInteger[i + 1];
            aa[i] = uu[i];
            bb[i] = 1;

            while (i != 0)
            {
                BigInteger aTop = uu[i - 1] * aa[i] + D * bb[i];
                BigInteger aBottom = BigInteger.Pow(aa[i], 2) + D * BigInteger.Pow(bb[i], 2);
                BigInteger nextA;
                if (aTop % aBottom != 0)
                {
                    aTop = D * bb[i] - uu[i - 1] * aa[i];
                }
                nextA = aTop / aBottom;

                BigInteger bTop = BigInteger.Negate(aa[i]) + uu[i - 1] * bb[i];
                BigInteger bBottom = BigInteger.Pow(aa[i], 2) + D * BigInteger.Pow(bb[i], 2);
                BigInteger nextB;
                if (bTop % bBottom != 0)
                {
                    bTop = BigInteger.Negate(aa[i]) - uu[i - 1] * bb[i];
                }
                nextB = bTop / bBottom;

                aa[i - 1] = nextA;
                bb[i - 1] = nextB;

                i--;
            }
            BigInteger a = BigInteger.Parse(aa[0].ToString());
            BigInteger b = BigInteger.Parse(bb[0].ToString());
            if (a < 0) a = BigInteger.Negate(a);
            if (b < 0) b = BigInteger.Negate(b);
            res[0] = a;
            res[1] = b;

            return res;
        }

        private bool QuadraticResidue(BigInteger B, BigInteger p)
        {
            BigInteger res = BigInteger.ModPow(B, (p - 1) / 2, p);
            if (res == 1) return true;
            return false;
        }

        private bool QuadraticNonResidue(BigInteger B, BigInteger p)
        {
            BigInteger res = BigInteger.ModPow(B, (p - 1) / 2, p);
            if (res == p - 1) return true;
            return false;
        }

        private bool CubicResidue(BigInteger B, BigInteger p)
        {
            BigInteger res = BigInteger.ModPow(B, (p - 1) / 3, p);
            if (res == 1) return true;
            return false;
        }

        private bool CubicNonResidue(BigInteger B, BigInteger p)
        {
            BigInteger res = BigInteger.ModPow(B, (p - 1) / 3, p);
            if (res != 1) return true;
            return false;
        }

        private BigInteger Nechto(BigInteger a, BigInteger p)
        {
            if (QuadraticNonResidue(a, p)) return -1;
            BigInteger b = 2;
            for (BigInteger j = 2; j < p; j++)
            {
                if (QuadraticNonResidue(j, p))
                {
                    b = j;
                    break;
                }
            }
            BigInteger pm1 = p - 1;
            BigInteger tmp = BigInteger.Parse(pm1.ToString());
            BigInteger t;
            int s = 0;
            while (tmp % 2 == 0)
            {
                s++;
                tmp = tmp / 2;
            }
            t = BigInteger.Parse(tmp.ToString());
            BigInteger a_inverse = Extended_GCD(a, p)[1];
            BigInteger c = BigInteger.ModPow(b, t, p);
            BigInteger r = BigInteger.ModPow(a, (t + 1)/2, p); 
            for (int i = 1; i <= s - 1; i++)
            {
                BigInteger tpw = BigInteger.Pow(2, s - i - 1); 
                BigInteger d = BigInteger.ModPow(BigInteger.Pow(r, 2) * a_inverse, tpw, p); 
                if (d == pm1) r = (r * c) % p;
                c = BigInteger.ModPow(c, 2, p); 
            }

            return r;
        }

        private BigInteger[] GetB(BigInteger p, BigInteger N, BigInteger r)
        {
            BigInteger[] b = new BigInteger[3];
            BigInteger B, x0, y0;

            Random random = new Random();

            do
            {
                string str = "";
                for (int j = 0; j < p.ToString().Length; j++)
                    str += random.Next() % 10;

                x0 = BigInteger.Parse(str) % p;

                if (x0 == 0) x0 = 1;

                str = "";
                for (int j = 0; j < p.ToString().Length; j++)
                    str += random.Next() % 10;

                y0 = BigInteger.Parse(str) % p;

                if (y0 == 0) y0 = 1;

                B = (BigInteger.Pow(y0, 2) - BigInteger.Pow(x0, 3)) % p;
            } while (!(((N == r) && ((QuadraticNonResidue(B, p)) && (CubicNonResidue(B, p)))) ||
                    ((N == 6 * r) && (QuadraticResidue(B, p) && (CubicResidue(B, p)))) ||
                    ((N == 2 * r) && (QuadraticNonResidue(B, p)) && (CubicResidue(B, p))) ||
                    ((N == 3 * r) && (QuadraticResidue(B, p)) && (CubicNonResidue(B, p)))));

            b[0] = B;
            b[1] = x0;
            b[2] = y0;
            return b;
        }

        private BigInteger GetStartPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            if (bits % 8 == 0)
            {
                tmp[bits / 8 - 1] = 0x80;
            }
            else
            {
                tmp[bits / 8] = (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetEndPosition(int bits)
        {
            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = 0xFF;
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((1 << (bits % 8)) - 1);
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetRandomPosition(int bits)
        {
            Random rand = new Random();

            byte[] tmp = new byte[bits / 8 + 1];

            for (int i = 0; i < bits / 8; i++)
            {
                tmp[i] = (byte)(rand.Next() % 0xFF);
            }

            if (bits % 8 != 0)
            {
                tmp[bits / 8] = (byte)((rand.Next() % ((1 << (bits % 8)) - 1)));
                tmp[bits / 8] |= (byte)(1 << (bits % 8 - 1));
            }

            return new BigInteger(tmp);
        }

        private BigInteger GetPrimeOverBits(int bits)
        {
            BigInteger startInterval = GetStartPosition(bits);
            BigInteger endInterval = GetEndPosition(bits);
            BigInteger startBusting = GetRandomPosition(bits);

            for (BigInteger i = startBusting; i < endInterval; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }
            for (BigInteger i = startInterval; i < startBusting; i++)
            {
                if (IsPrime(i))
                {
                    return i;
                }
            }

            return BigInteger.Parse("-1");
        }

        private bool IsPrime(BigInteger p)
        {
            BigInteger p1 = p - 1;

            int s = 0;

            BigInteger t = p1;
            while (t % 2 == 0)
            {
                t /= 2;
                s++;
            }

            if (s == 0)
            {
                return false;
            }

            for (int i = 0; i < 30; i++)
            {
                Random rand = new Random();
                BigInteger tmp = 0;
                while (tmp > p1 || tmp < 2)
                {
                    string str = "";
                    for (int j = 0; j < p1.ToString().Length; j++)
                        str += rand.Next() % 10;

                    tmp = BigInteger.Parse(str);
                    tmp %= p;
                }

                BigInteger x = BigInteger.ModPow(tmp, t, p);

                if (x <= 1 || x == p1)
                {
                    continue;
                }

                bool f = false;
                for (int j = 0; j < s - 1; j++)
                {
                    x = BigInteger.ModPow(x, 2, p);

                    if (x == 1)
                    {
                        return false;
                    }

                    if (x == p1)
                    {
                        f = true;
                        break;
                    }
                }

                if (!f)
                {
                    return false;
                }
            }
            return true;
        }

        public static BigInteger[] Extended_GCD(BigInteger A, BigInteger B)
        {
            BigInteger[] result = new BigInteger[3];
            bool reverse = false;
            if (A < B)
            {
                BigInteger temp = A;
                A = B;
                B = temp;
                reverse = true;
            }
            BigInteger r = B;
            BigInteger q = 0;
            BigInteger x0 = 1;
            BigInteger y0 = 0;
            BigInteger x1 = 0;
            BigInteger y1 = 1;
            BigInteger x = 0, y = 0;
            while (A % B != 0)
            {
                r = A % B;
                q = A / B;
                x = x0 - q * x1;
                y = y0 - q * y1;
                x0 = x1;
                y0 = y1;
                x1 = x;
                y1 = y;
                A = B;
                B = r;
            }
            result[0] = r;
            if (reverse)
            {
                result[1] = y;
                result[2] = x;
            }
            else
            {
                result[1] = x;
                result[2] = y;
            }
            return result;
        }
    }
}
